EESchema Schematic File Version 4
LIBS:Sensor_SDS_I2C_I2S-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "TTGO-TBeam_Sensor_SDS_BME"
Date "2019-09-09"
Rev "1"
Comp "Grünstreifen eV"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 3900 3700
Wire Wire Line
	5250 4400 5250 4350
NoConn ~ 5950 3150
NoConn ~ 5950 3250
NoConn ~ 5950 4150
NoConn ~ 5950 4250
NoConn ~ 4950 4150
NoConn ~ 4950 3850
NoConn ~ 4950 3650
NoConn ~ 4950 3550
NoConn ~ 4950 3450
NoConn ~ 4950 3350
NoConn ~ 4950 3250
NoConn ~ 4950 3150
Text GLabel 4950 4350 0    50   Input ~ 0
VCC
Text GLabel 3900 3600 2    50   Input ~ 0
VCC
Text GLabel 5950 3650 2    50   Input ~ 0
GND
Text GLabel 7050 3350 0    50   Input ~ 0
GND
Text GLabel 7050 3450 0    50   Input ~ 0
VCC
Text GLabel 7050 3550 0    50   Input ~ 0
SCL
Text GLabel 5950 3950 2    50   Input ~ 0
SCL
Text GLabel 7050 3650 0    50   Input ~ 0
SDA
Text GLabel 5950 4050 2    50   Input ~ 0
SDA
$Comp
L Connector:Conn_01x13_Female J3
U 1 1 5D7892FD
P 5750 3750
F 0 "J3" H 5400 4400 50  0000 C CNN
F 1 "TTGO" H 5650 4550 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 5750 3750 50  0001 C CNN
F 3 "~" H 5750 3750 50  0001 C CNN
	1    5750 3750
	-1   0    0    -1  
$EndComp
Text GLabel 3900 3500 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x13_Female J2
U 1 1 5D78D7ED
P 5150 3750
F 0 "J2" H 4750 4400 50  0000 L CNN
F 1 "TTGO" H 5000 4550 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 5150 3750 50  0001 C CNN
F 3 "~" H 5150 3750 50  0001 C CNN
	1    5150 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5D79051D
P 7250 3450
F 0 "J4" H 7000 3800 50  0000 L CNN
F 1 "I2C_BME" H 6900 3700 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 7250 3450 50  0001 C CNN
F 3 "~" H 7250 3450 50  0001 C CNN
	1    7250 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Female J1
U 1 1 5D793739
P 3700 3500
F 0 "J1" H 3592 3885 50  0000 C CNN
F 1 "Serial_SDS011" H 3592 3794 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B5B-XH-A_1x05_P2.50mm_Vertical" H 3700 3500 50  0001 C CNN
F 3 "~" H 3700 3500 50  0001 C CNN
	1    3700 3500
	-1   0    0    -1  
$EndComp
Text Notes 5200 4400 0    63   ~ 0
36\n39\nRST\n34\n35\n32\n33\n25\n14\n13\n02\nGND\n5V
Text Notes 5500 4400 0    63   ~ 0
TXD\nRXD\n23\n4\n0\nGND\n3.3V\nGND\n22\n21\n3.3\nLora2\nLora1
Text GLabel 4950 4050 0    50   Input ~ 0
SDS_RX
Text GLabel 3900 3400 2    50   Input ~ 0
SDS_RX
Text GLabel 4950 3950 0    50   Input ~ 0
SDS_TX
Text GLabel 3900 3300 2    50   Input ~ 0
SDS_TX
Text Notes 5250 4600 0    50   ~ 0
TTGO-TBeam
Wire Notes Line
	5850 3050 5850 4450
Wire Notes Line
	5850 4450 5050 4450
Wire Notes Line
	5050 4450 5050 3050
Wire Notes Line
	5050 3050 5850 3050
Wire Wire Line
	5950 4350 6100 4350
Wire Wire Line
	6100 4350 6100 4750
Wire Wire Line
	6100 4750 4450 4750
Wire Wire Line
	4450 4750 4450 3750
Wire Wire Line
	4450 3750 4950 3750
Text GLabel 5950 3750 2    50   Input ~ 0
3p3V
Text GLabel 5950 3550 2    50   Input ~ 0
I2S_WC
Text GLabel 5950 3450 2    50   Input ~ 0
I2S_SCK
Text GLabel 5950 3350 2    50   Input ~ 0
I2S_SD
Text GLabel 5950 3850 2    50   Input ~ 0
GND
Text GLabel 4950 4250 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x10_Female J7
U 1 1 5DE5D6A7
P 7250 4500
F 0 "J7" H 7142 3775 50  0000 C CNN
F 1 "I2C_ADS1115" H 7142 3866 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Vertical" H 7250 4500 50  0001 C CNN
F 3 "~" H 7250 4500 50  0001 C CNN
	1    7250 4500
	1    0    0    1   
$EndComp
Text GLabel 7050 4000 0    50   Input ~ 0
ADC3
Text GLabel 7050 4100 0    50   Input ~ 0
ADC2
Text GLabel 7050 4200 0    50   Input ~ 0
ADC1
Text GLabel 7050 4300 0    50   Input ~ 0
ADC0
NoConn ~ 7050 4400
Text GLabel 7050 4500 0    50   Input ~ 0
GND
Text GLabel 7050 4600 0    50   Input ~ 0
SDA
Text GLabel 7050 4700 0    50   Input ~ 0
SCL
Text GLabel 7050 4800 0    50   Input ~ 0
GND
Text GLabel 7050 4900 0    50   Input ~ 0
VCC
$Comp
L Connector:Conn_01x03_Male J8
U 1 1 5DE640AC
P 8250 3500
F 0 "J8" H 8222 3432 50  0000 R CNN
F 1 "ADC0" H 8222 3523 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 8250 3500 50  0001 C CNN
F 3 "~" H 8250 3500 50  0001 C CNN
	1    8250 3500
	1    0    0    1   
$EndComp
NoConn ~ 8250 3400
NoConn ~ 8250 3500
NoConn ~ 8250 3600
Text GLabel 8450 3600 2    50   Input ~ 0
ADC0
Text GLabel 8450 3500 2    50   Input ~ 0
GND
Text GLabel 8450 3400 2    50   Input ~ 0
VCC
$Comp
L Connector:Conn_01x03_Male J9
U 1 1 5DE68CB7
P 8250 3950
F 0 "J9" H 8222 3882 50  0000 R CNN
F 1 "ADC1" H 8222 3973 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 8250 3950 50  0001 C CNN
F 3 "~" H 8250 3950 50  0001 C CNN
	1    8250 3950
	1    0    0    1   
$EndComp
NoConn ~ 8250 3850
NoConn ~ 8250 3950
NoConn ~ 8250 4050
Text GLabel 8450 3850 2    50   Input ~ 0
VCC
Text GLabel 8450 3950 2    50   Input ~ 0
GND
Text GLabel 8450 4050 2    50   Input ~ 0
ADC1
$Comp
L Connector:Conn_01x03_Male J10
U 1 1 5DE69DB9
P 8250 4400
F 0 "J10" H 8222 4332 50  0000 R CNN
F 1 "ADC2" H 8222 4423 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 8250 4400 50  0001 C CNN
F 3 "~" H 8250 4400 50  0001 C CNN
	1    8250 4400
	1    0    0    1   
$EndComp
Text GLabel 8450 4300 2    50   Input ~ 0
VCC
Text GLabel 8450 4400 2    50   Input ~ 0
GND
Text GLabel 8450 4500 2    50   Input ~ 0
ADC2
NoConn ~ 8250 4300
NoConn ~ 8250 4400
NoConn ~ 8250 4500
$Comp
L Connector:Conn_01x03_Male J11
U 1 1 5DE6B07C
P 8250 4850
F 0 "J11" H 8222 4782 50  0000 R CNN
F 1 "ADC3" H 8222 4873 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 8250 4850 50  0001 C CNN
F 3 "~" H 8250 4850 50  0001 C CNN
	1    8250 4850
	1    0    0    1   
$EndComp
NoConn ~ 8250 4750
NoConn ~ 8250 4850
NoConn ~ 8250 4950
Text GLabel 8450 4750 2    50   Input ~ 0
VCC
Text GLabel 8450 4850 2    50   Input ~ 0
GND
Text GLabel 8450 4950 2    50   Input ~ 0
ADC3
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 5DE7D070
P 7250 2700
F 0 "J6" H 7000 3050 50  0000 L CNN
F 1 "I2C_Display" H 6800 2950 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 7250 2700 50  0001 C CNN
F 3 "~" H 7250 2700 50  0001 C CNN
	1    7250 2700
	1    0    0    -1  
$EndComp
Text GLabel 7050 2600 0    50   Input ~ 0
GND
Text GLabel 7050 2700 0    50   Input ~ 0
VCC
Text GLabel 7050 2800 0    50   Input ~ 0
SCL
Text GLabel 7050 2900 0    50   Input ~ 0
SDA
$Comp
L Connector:Conn_01x06_Male J5
U 1 1 5DE857F2
P 3700 4350
F 0 "J5" H 3808 4731 50  0000 C CNN
F 1 "I2S_Micro" H 3808 4640 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B6B-XH-A_1x06_P2.50mm_Vertical" H 3700 4350 50  0001 C CNN
F 3 "~" H 3700 4350 50  0001 C CNN
	1    3700 4350
	1    0    0    -1  
$EndComp
Text GLabel 3900 4150 2    50   Input ~ 0
GND
Text GLabel 3900 4250 2    50   Input ~ 0
3p3V
Text GLabel 3900 4350 2    50   Input ~ 0
I2S_SD
Text GLabel 3900 4450 2    50   Input ~ 0
I2S_SCK
Text GLabel 3900 4550 2    50   Input ~ 0
I2S_WC
Text GLabel 3900 4650 2    50   Input ~ 0
GND
NoConn ~ 3700 4150
NoConn ~ 3700 4250
NoConn ~ 3700 4350
NoConn ~ 3700 4450
NoConn ~ 3700 4550
NoConn ~ 3700 4650
$EndSCHEMATC
